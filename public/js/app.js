// Julian Kandlhofer, 1 2020
//
let points = []

let contentRect;

const dt = 0.5;
const rows = 5;
const cols = 8;

class Point {

  constructor(x, y) {
    // random position
    //
    // draw grid with slightly random offsets
    this.pos = createVector(x, y);

    // random velocity
    this.size =  5 * Math.random() + 5;
    this.vel = createVector((Math.random() * 2) - 1, (Math.random() * 2) - 1);
    this.vel.mult(0.5 * this.size / 5);
    this.omega = Math.PI * (Math.random() *2 -1) * 0.05

    // random size

    const shade = Math.random() * 200 * this.size * 0.3;

    // random color
    this.color = {r: shade, g: shade, b:shade};

    this.tail = []
    this.loopCount = 0;
  }

  update(dt, rect) {
    // window bounds
    if (this.pos.x < 0 || this.pos.x > window.innerWidth) {
      this.vel.x *= -1;
    }

    if (this.pos.y < 0 || this.pos.y > window.innerHeight) {
      this.vel.y *= -1;
    }

    this.pos.add(p5.Vector.mult(this.vel, dt))
    this.updateTail()
  }

  
  updateTail() {
    this.loopCount++
    if(this.loopCount < 10) {
      return
    }
    this.loopCount = 0;
    this.tail.unshift(this.pos.copy())

    if(this.tail.length > 15) {
      this.tail.pop()
    }
  }

  draw() {
    for(let i = 0; i < this.tail.length - 1; i++) {
      const factor  = 1 - (i / this.tail.length)
      strokeWeight(this.size * factor * 0.5)
      stroke(this.color.r * factor * 0.7)
      line(this.tail[i].x, this.tail[i].y, this.tail[i+1].x, this.tail[i+1].y)
    }

    strokeWeight(0)
    fill(this.color.r, this.color.g, this.color.b)
    ellipse(this.pos.x, this.pos.y, this.size)

  }
}

function init() {
  const space_x = window.innerWidth / cols;
  const space_y = window.innerHeight / rows;

  points = []
  for(let x= 0; x < cols; x++) {
    for(let y= 0; y < rows; y++) {
      const variation_x = Math.random() * space_x;
      const variation_y = Math.random() * space_y;
      points.push(new Point(
        (x * space_x) + (space_x / 2) + variation_x,
        (y * space_y) + (space_y / 2) + variation_y
      ));
    }
  }
}


let resizeTimeout;
function setup() {
  frameRate(60);
  contentRect = document.querySelector(".outer").getBoundingClientRect();
  setTimeout(() => {
    contentRect = document.querySelector(".outer").getBoundingClientRect();
  },0)

  window.addEventListener("resize", e =>  {
    if(resizeTimeout)
      clearTimeout(resizeTimeout)

    resizeTimeout = setTimeout(() => resizedWindow(e), 200);

  });

  contentRect = document.querySelector("body").getBoundingClientRect();
  createCanvas(contentRect.width, contentRect.height);

  init();
}

function resizedWindow(e) {
  contentRect = document.querySelector("body").getBoundingClientRect();
  resizeCanvas(contentRect.width, contentRect.height);
  init()
}

function draw() {
  background(15);

  for(const pnt of points) {
    pnt.update(dt, contentRect)
    pnt.draw()
  }
}
